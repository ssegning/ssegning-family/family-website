# More Help from https://nodejs.org/de/docs/guides/nodejs-docker-webapp/
FROM node:14-alpine AS BUILD_IMAGE

WORKDIR /app

COPY . .

RUN yarn --frozen-lockfile

RUN yarn build

FROM nginxinc/nginx-unprivileged

LABEL maintener="Stephane Segning <selastlambou@gmail.com>"

WORKDIR /usr/share/nginx/

# copy from build image
COPY --from=BUILD_IMAGE /app/dist ./html
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
