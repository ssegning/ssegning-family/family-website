import {Component, OnDestroy, OnInit} from '@angular/core';
import {CreateFamilyGQL} from '@generated/graphql';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {map} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit, OnDestroy {

  public formGroup: FormGroup;
  public step = 0;
  private readonly subscription = new Subscription();

  constructor(
    private readonly createFamily: CreateFamilyGQL,
    private readonly fb: FormBuilder,
    private readonly ref: MatDialogRef<AddFamilyComponent>,
  ) {
  }

  setStep(index: number): void {
    this.step = index;
  }

  nextStep(): void {
    this.step++;
  }

  prevStep(): void {
    this.step--;
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      description: ['', []],
      displayName: ['', [Validators.required]],
      name: ['', [Validators.required]], // TODO Add pattern here
      avatar: this.fb.group({
        avatarUrl: [],
        width: [1],
        height: [1],
      })
    });

    const sub = this.formGroup
      .controls
      .displayName
      .valueChanges
      .subscribe((value: string) => {
        if ((!value) || (typeof value !== 'string')) {
          return;
        }

        this.formGroup
          .controls
          .description
          .setValue(`This family's space is designed to the ${value}'s family`);

        const cleanName = value.replace(/[^0-9-.a-z]/ig, '-');
        if (cleanName !== '') {
          const suffix = Math.random().toString(36).substring(7).toLowerCase();
          this.formGroup.controls.name.setValue(`${cleanName.toLowerCase()}-${suffix}`);
        }
      });
    this.subscription.add(sub);
  }

  getErrors(name: string): ValidationErrors | null {
    return this.formGroup.controls[name].errors;
  }

  async save(): Promise<void> {
    if (this.formGroup.valid) {
      const {name, displayName, description, avatar} = this.formGroup.value;
      const variables: any = {
        name, displayName, description,
      };
      if (!!avatar.avatarUrl) {
        variables.avatar = avatar;
      }

      await this.createFamily
        .mutate(variables)
        .pipe(
          map(({data: {family}}) => this.ref.close(family))
        )
        .toPromise();
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
