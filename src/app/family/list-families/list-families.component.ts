import {Component, OnDestroy, OnInit} from '@angular/core';
import {map} from 'rxjs/operators';
import {AccountMemberShipsGQL, AccountMemberShipsSubscription, GetFamilyMembershipsGQL} from '@generated/graphql';
import {MatDialog} from '@angular/material/dialog';
import {AddFamilyComponent} from '../add-family/add-family.component';
import {merge, Subscription} from 'rxjs';
import * as _ from 'lodash';

function NoopForType() {
  const meh: AccountMemberShipsSubscription = {member: undefined};
  return meh.member;
}

@Component({
  selector: 'app-list-families',
  templateUrl: './list-families.component.html',
  styleUrls: ['./list-families.component.scss']
})
export class ListFamiliesComponent implements OnInit, OnDestroy {

  public memberships: ReturnType<typeof NoopForType>[] = [];
  private readonly subscription = new Subscription();

  constructor(
    private readonly membershipsGQL: GetFamilyMembershipsGQL,
    private readonly accountMemberShipsGQL: AccountMemberShipsGQL,
    private readonly matDialog: MatDialog
  ) {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    const sub = merge(
      this.membershipsGQL.fetch().pipe(map(({data: {mShips}}) => mShips)),
      this.accountMemberShipsGQL.subscribe().pipe(map(({data: {member}}) => [member])),
    )
      .subscribe((list) => {
        if (list) {
          this.memberships.push(...list);
          this.memberships = _.uniqBy(this.memberships, 'id');
        }
      });
    this.subscription.add(sub);
  }

  public addFamily(): boolean {
    const dialogRef = this.matDialog.open(AddFamilyComponent, {
      maxWidth: '600px',
      width: '90%'
    });
    dialogRef.afterClosed().subscribe((familyAdded: any) => {
      console.log({familyAdded});
    });

    return true;
  }

  public joinFamily(): boolean {

    return true;
  }
}
