import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {FamilyBodyFragment, GetFamilyGQL} from '@generated/graphql';
import {map} from 'rxjs/operators';
import {FamilyContextHolderService} from '@shared/service/family-context-holder.service';

export interface FamilyResolved {
  family: FamilyBodyFragment;
}

@Injectable({
  providedIn: 'root'
})
export class FamilyResolver implements Resolve<FamilyResolved> {

  constructor(
    private readonly getFamilyGQL: GetFamilyGQL,
    private readonly familyContextHolderService: FamilyContextHolderService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FamilyResolved> {
    const familyName: string = route.params.familyName;
    return this.getFamilyGQL
      .fetch({id: familyName})
      .pipe(
        map(({data: {family}}) => {
          const resolved: FamilyResolved = {family};
          this.familyContextHolderService.setContext(resolved);
          return resolved;
        })
      );
  }

}
