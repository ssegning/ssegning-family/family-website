import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FamilyComponent} from './family.component';
import {ListFamiliesComponent} from './list-families/list-families.component';

const children: Routes = [
  {
    component: ListFamiliesComponent,
    path: ''
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
];

const routes: Routes = [{
  component: FamilyComponent,
  path: '',
  children
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FamilyRoutingModule {
}
