import {NgModule} from '@angular/core';

import {FamilyRoutingModule} from './family-routing.module';
import {FamilyComponent} from './family.component';
import {SharedModule} from '@shared/shared.module';
import {AddFamilyComponent} from './add-family/add-family.component';
import {ListFamiliesComponent} from './list-families/list-families.component';
import {FamilyInfoComponent} from './family-info/family-info.component';


@NgModule({
  declarations: [FamilyComponent, AddFamilyComponent, ListFamiliesComponent, FamilyInfoComponent],
  imports: [
    SharedModule,
    FamilyRoutingModule,
  ],
})
export class FamilyModule {
}
