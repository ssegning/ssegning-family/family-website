export interface GraphqlConfig {
  graphqlUri: string;
  graphqlSub: string;
}
