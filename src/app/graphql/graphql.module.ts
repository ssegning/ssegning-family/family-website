import {APOLLO_OPTIONS} from 'apollo-angular';
import {HttpLink} from 'apollo-angular/http';
import {ApolloClientOptions, ApolloLink, InMemoryCache, split} from '@apollo/client/core';
import {WebSocketLink} from '@apollo/client/link/ws';
import {getMainDefinition} from '@apollo/client/utilities';
import {onError} from '@apollo/client/link/error';
import {GraphQLConfigService} from './graph-q-l-config.service';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {setContext} from '@apollo/client/link/context';
import {createPersistedQueryLink} from 'apollo-angular/persisted-queries';
import {sha256} from 'crypto-hash';
import {LocalForageWrapper, persistCache} from 'apollo3-cache-persist';
import * as localforage from 'localforage';
import {FamilyContextHolderService} from '@shared/service/family-context-holder.service';

export const apolloCache = new InMemoryCache();

export const initCache = async () => {
  await persistCache({
    cache: apolloCache,
    storage: new LocalForageWrapper(localforage),
    key: 'mifa-graphql-storage',
  });
};

const errorLink = onError(({graphQLErrors, networkError}) => {
  if (graphQLErrors) {
    graphQLErrors.map(({message, locations, path}) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
      ),
    );
  }

  if (networkError) {
    console.log(`[Network error]: ${networkError.name}: ${networkError.message}`);
  }
});

export function createApollo(
  httpLink: HttpLink,
  graphqlConfigService: GraphQLConfigService,
  keycloakService: KeycloakService,
  familyContextHolderService: FamilyContextHolderService,
): ApolloClientOptions<any> {
  const {graphqlUri, graphqlSub} = graphqlConfigService.getConfig();
  const auth = setContext(async () => {
    if (keycloakService.isTokenExpired(50)) {
      await keycloakService.updateToken();
    }
    const token = await keycloakService.getToken();
    const familyResolved = familyContextHolderService.value;
    const headers: any = {
      Authorization: `Bearer ${token}`,
    };
    if (familyResolved?.family?.name) {
      headers['X-APP-FAMILY-NAME'] = familyResolved?.family?.name;
    }
    return {
      headers,
    };
  });
  const http = ApolloLink.from([
    createPersistedQueryLink({sha256}),
    auth,
    httpLink.create({uri: graphqlUri}),
  ]);
  const ws = ApolloLink.from([
    auth,
    new WebSocketLink({
      uri: graphqlSub,
      options: {
        reconnect: true,
        connectionParams: async () => {
          if (keycloakService.isTokenExpired(50)) {
            await keycloakService.updateToken();
          }
          return {
            authToken: await keycloakService.getToken()
          };
        }
      },
    })
  ]);
  const link = ApolloLink.from([
    errorLink,
    split(
      // split based on operation type
      ({query}) => {
        const mainDef = getMainDefinition(query);
        return (
          mainDef.kind === 'OperationDefinition' && mainDef.operation === 'subscription'
        );
      },
      ws,
      http,
    )
  ]);

  return ({
    link, cache: apolloCache,
    credentials: 'include',
    defaultOptions: {
      query: {fetchPolicy: 'network-only'},
      mutate: {fetchPolicy: 'no-cache'},
      watchQuery: {fetchPolicy: 'no-cache'}
    }
  });
}

function configFactory(graphQLConfigService: GraphQLConfigService): any {
  return async () => {
    await initCache();
    await graphQLConfigService.init();
  };
}

@NgModule({
  imports: [],
  providers: [
    GraphQLConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configFactory,
      deps: [GraphQLConfigService],
      multi: true,
    },
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink, GraphQLConfigService, KeycloakService, FamilyContextHolderService],
    },
  ],
})
export class GraphQLModule {
}
