import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GraphqlConfig} from './graphql.config';

@Injectable()
export class GraphQLConfigService {
  private config: GraphqlConfig;

  constructor(private readonly http: HttpClient) {
  }

  async init(): Promise<void> {
    const graphQL = this.http.get<GraphqlConfig>('/assets/config/graphql.json');
    this.config = await graphQL.toPromise();
  }

  getConfig(): GraphqlConfig {
    return this.config;
  }
}
