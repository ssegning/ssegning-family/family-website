import { TestBed } from '@angular/core/testing';

import { GraphQLConfigService } from './graph-q-l-config.service';

describe('GraphQLConfigService', () => {
  let service: GraphQLConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GraphQLConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
