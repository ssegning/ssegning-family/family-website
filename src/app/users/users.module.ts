import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users/users.component';
import { UserListComponent } from './user-list/user-list.component';
import {SharedModule} from '@shared/shared.module';


@NgModule({
  declarations: [UsersComponent, UserListComponent],
    imports: [
        CommonModule,
        UsersRoutingModule,
        SharedModule
    ]
})
export class UsersModule { }
