import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {UserListComponent} from './user-list/user-list.component';

const children: Routes = [
  {
    component: UserListComponent,
    path: ''
  },
  {
    pathMatch: 'full',
    path: '**',
    redirectTo: ''
  }
];

const routes: Routes = [{
  component: UsersComponent,
  path: '',
  children
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
