import {Component, OnInit} from '@angular/core';
import {GetFamilyMembersGQL} from '@generated/graphql';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  public members$ = this.familyMembersGQL
    .watch()
    .valueChanges
    .pipe(map(v => v.data?.members));

  constructor(
    protected readonly familyMembersGQL: GetFamilyMembersGQL
  ) {
  }

  ngOnInit(): void {
  }

}
