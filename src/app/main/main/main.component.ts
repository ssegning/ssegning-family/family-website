import {Component, OnInit} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {FamilyContextHolderService} from '@shared/service/family-context-holder.service';
import {FamilyBodyFragment} from '@generated/graphql';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(
    protected readonly keycloakService: KeycloakService,
    protected readonly familyContextHolderService: FamilyContextHolderService,
  ) {
  }

  get family(): FamilyBodyFragment {
    return this.familyContextHolderService.value?.family;
  }

  ngOnInit(): void {
  }

  async logout(): Promise<void> {
    await this.keycloakService.logout(window.location.origin);
  }

}
