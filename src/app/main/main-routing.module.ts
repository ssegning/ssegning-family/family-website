import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main/main.component';
import {AuthGuard} from '@shared/guard/auth.guard';
import {DashboardComponent} from './dashboard/dashboard.component';

const children: Routes = [
  {
    loadChildren: () => import('../albums/albums.module').then(m => m.AlbumsModule),
    path: 'albums',
    data: {
      role: []
    },
  },
  {
    loadChildren: () => import('../photos/photos.module').then(m => m.PhotosModule),
    path: 'photos',
    data: {
      role: []
    },
  },
  {
    loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule),
    path: 'settings',
    data: {
      role: []
    },
  },
  {
    loadChildren: () => import('../users/users.module').then(m => m.UsersModule),
    path: 'users',
    data: {
      role: []
    },
  },
  {
    component: DashboardComponent,
    path: 'home',
    canActivate: [AuthGuard],
    data: {
      role: []
    }
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'photos'
  }
];

const routes: Routes = [{
  component: MainComponent,
  path: '',
  children,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
