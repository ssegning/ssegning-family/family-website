import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MainRoutingModule} from './main-routing.module';
import {MainComponent} from './main/main.component';
import {FooterComponent} from './component/footer/footer.component';
import {SharedModule} from '@shared/shared.module';
import {DashboardComponent} from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    MainComponent,
    FooterComponent,
    DashboardComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule
  ]
})
export class MainModule {
}
