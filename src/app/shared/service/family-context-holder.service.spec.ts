import { TestBed } from '@angular/core/testing';

import { FamilyContextHolderService } from './family-context-holder.service';

describe('FamilyContextHolderService', () => {
  let service: FamilyContextHolderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FamilyContextHolderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
