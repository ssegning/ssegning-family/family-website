import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {FamilyResolved} from '@family/resolvers/family.resolver';

@Injectable({
  providedIn: 'root'
})
export class FamilyContextHolderService {

  private readonly subject = new BehaviorSubject<FamilyResolved>(null);

  setContext(resolved?: FamilyResolved): void {
    this.subject.next(resolved);
  }

  getContext(): Observable<FamilyResolved> {
    return this.subject.asObservable();
  }

  get value(): FamilyResolved {
    return this.subject.getValue();
  }
}
