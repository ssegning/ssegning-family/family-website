import {NgModule} from '@angular/core';
import {AuthGuard} from './guard/auth.guard';
import {RouterModule} from '@angular/router';
import {MaterialModule} from './material.module';
import {MediaCardComponent} from './components/media-card/media-card.component';
import {CommonModule} from '@angular/common';
import { ZoomModalComponent } from './components/zoom-modal/zoom-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import { AddMediaComponent } from './components/add-media/add-media.component';
import { UploadSurfaceComponent } from './components/upload-surface/upload-surface.component';
import { FileSizePipe } from './pipe/file-size.pipe';
import {GraphQLModule} from '@graphql/graphql.module';

@NgModule({
  declarations: [
    MediaCardComponent,
    ZoomModalComponent,
    AddMediaComponent,
    UploadSurfaceComponent,
    FileSizePipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
    GraphQLModule
  ],
  providers: [
    AuthGuard
  ],
  exports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    MediaCardComponent,
    FileSizePipe
  ]
})
export class SharedModule {
}
