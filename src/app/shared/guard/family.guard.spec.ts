import { TestBed } from '@angular/core/testing';

import { FamilyGuard } from './family.guard';

describe('FamilyGuard', () => {
  let guard: FamilyGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(FamilyGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
