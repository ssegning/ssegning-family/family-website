import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {LocalForageService} from 'ngx-localforage';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FamilyGuard implements CanActivate {

  constructor(
    private readonly localForageService: LocalForageService,
    private readonly router: Router,
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.hasFamily()
      .pipe(tap(valid => {
        if (!valid) {
          this.router.navigate(['/family']);
        }
      }))
      .toPromise();
  }

  hasFamily(): Observable<boolean> {
    return this.localForageService.getItem('familyName')
      .pipe(
        tap(console.log),
        map(value => !!value),
        catchError(err => of(false)),
      );
  }
}
