import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ZoomModalComponent} from '@shared/components/zoom-modal/zoom-modal.component';
import {GetMediasQuery} from '@generated/graphql';

@Component({
  selector: 'app-media-card',
  templateUrl: './media-card.component.html',
  styleUrls: ['./media-card.component.scss']
})
export class MediaCardComponent implements OnInit {

  @Input()
  public media: GetMediasQuery['medias'][0];

  @Input()
  public width: number;

  constructor(
    public readonly dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
  }

  openModal(): boolean {
    const dialogRef = this.dialog.open(ZoomModalComponent, {
      data: this.media,
      minHeight: '80%',
      maxWidth: '80%',
      minWidth: '50%',
    });
    return true;
  }

}
