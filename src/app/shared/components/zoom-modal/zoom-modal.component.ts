import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {GetMediasQuery} from '@generated/graphql';

export interface ZoomMediaModal extends Record<string, any> {
}

@Component({
  selector: 'app-zoom-modal',
  templateUrl: './zoom-modal.component.html',
  styleUrls: ['./zoom-modal.component.scss']
})
export class ZoomModalComponent implements OnInit {

  constructor(
    public readonly dialogRef: MatDialogRef<ZoomModalComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly data: GetMediasQuery['medias'][0]) {
  }

  ngOnInit(): void {
  }

}
