import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {FileInput} from '@shared/components/upload-surface/upload-surface.component';
import {CreateMediaGQL, MediaBodyFragment} from '@generated/graphql';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-add-media',
  templateUrl: './add-media.component.html',
  styleUrls: ['./add-media.component.scss']
})
export class AddMediaComponent implements OnInit, OnDestroy {

  private readonly mediasUploaded: Set<MediaBodyFragment> = new Set();
  private readonly subscription = new Subscription();

  constructor(
    public readonly dialogRef: MatDialogRef<AddMediaComponent>,
    private readonly createMediaGQL: CreateMediaGQL
  ) {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
  }

  upload = (input: FileInput): void => {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];

    const d = new Date();

    const sub = this.createMediaGQL
      .mutate({
        file: input.file,
        input: {
          albumIds: [],
          description: 'This image was upload in ' + monthNames[d.getMonth()],
          referencedMembers: [],
          tags: [monthNames[d.getMonth()]]
        }
      }, {
        context: {
          useMultipart: true
        }
      })
      .subscribe(({data: {media}}) => {
        this.mediasUploaded.add(media);
      });
    this.subscription.add(sub);
  };

}
