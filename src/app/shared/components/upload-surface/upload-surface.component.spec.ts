import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadSurfaceComponent } from './upload-surface.component';

describe('UploadSurfaceComponent', () => {
  let component: UploadSurfaceComponent;
  let fixture: ComponentFixture<UploadSurfaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadSurfaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadSurfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
