import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {DomSanitizer, SafeValue} from '@angular/platform-browser';

const toBase64 = (file: File) => new Promise<string>((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result as string);
  reader.onerror = error => reject(error);
});

export class FileInput {
  progress: number;
  canBeClosed = true;
  showCancelButton = true;
  uploading = false;
  uploadSuccessful = false;

  constructor(
    public readonly file: File,
    public readonly fileUrl: SafeValue,
  ) {
  }

}

@Component({
  selector: 'app-upload-surface',
  templateUrl: './upload-surface.component.html',
  styleUrls: ['./upload-surface.component.scss']
})
export class UploadSurfaceComponent {

  @ViewChild('fileInput') file: ElementRef<HTMLInputElement>;

  public files: Set<FileInput> = new Set();

  @Input()
  public mimeType: string[] = ['image/*'];

  @Input()
  public upload: (file: FileInput) => void | Observable<void> | Promise<void>;

  @Output()
  private onUploadOne = new EventEmitter<FileInput>();

  constructor(
    private readonly domSanitizer: DomSanitizer
  ) {
  }

  onFilesAdded(): void {
    const files: FileList = this.file.nativeElement.files;
    for (let i = 0; i < files.length; i++) {
      (async () => {
        const file = files.item(i);
        const url = await toBase64(file);
        this.files.add(new FileInput(file, this.domSanitizer.bypassSecurityTrustUrl(url)));
      })();
    }
  }

  addFiles(): void {
    this.file.nativeElement.click();
  }

  uploadOne(fileInput: FileInput): void {
    if (this.upload && typeof this.upload === 'function') {
      this.upload(fileInput);
    } else {
      this.onUploadOne.emit(fileInput);
    }
  }

  removeOne(fileInput: FileInput): void {
    this.files.delete(fileInput);
  }

  uploadAll(): void {
    this.files.forEach((fileInput) => this.uploadOne(fileInput));
  }
}
