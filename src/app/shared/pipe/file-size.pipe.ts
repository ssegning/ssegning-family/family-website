import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {

  transform(bytes: number, precision = 2, ...args: unknown[]): unknown {
    const units = ['B', 'KB', 'MB', 'GB', 'TB'];

    bytes = Math.max(bytes, 0);
    let pow = Math.floor((bytes ? Math.log(bytes) : 0) / Math.log(1024));
    pow = Math.min(pow, units.length - 1);

    bytes /= Math.pow(1024, pow);

    return `${bytes.toFixed(precision)} ${units[pow]}`;
  }

}
