import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PhotosRoutingModule} from './photos-routing.module';
import {PhotosComponent} from './photos/photos.component';
import {ListPhotosComponent} from './list-photos/list-photos.component';
import {SinglePhotoComponent} from './single-photo/single-photo.component';
import {PhotoDetailComponent} from './photo-detail/photo-detail.component';
import {SharedModule} from '@shared/shared.module';
import {NgxMasonryModule} from 'ngx-masonry';


@NgModule({
  declarations: [PhotosComponent, ListPhotosComponent, SinglePhotoComponent, PhotoDetailComponent],
  imports: [
    NgxMasonryModule,
    CommonModule,
    PhotosRoutingModule,
    SharedModule
  ]
})
export class PhotosModule {
}
