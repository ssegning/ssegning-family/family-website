import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AddMediaComponent} from '@shared/components/add-media/add-media.component';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {

  constructor(
    private readonly matDialog: MatDialog
  ) {
  }

  ngOnInit(): void {
  }

  public uploadMedia(): void {
    this.matDialog.open(AddMediaComponent, {
      width: '90%',
      height: '90%',
      maxWidth: '768px',
    });
  }
}
