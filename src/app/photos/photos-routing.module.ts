import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PhotosComponent} from './photos/photos.component';
import {ListPhotosComponent} from './list-photos/list-photos.component';
import {PhotoDetailComponent} from './photo-detail/photo-detail.component';

const children: Routes = [
  {
    component: PhotoDetailComponent,
    path: ':id',
  },
  {
    component: ListPhotosComponent,
    path: ''
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

const routes: Routes = [{
  component: PhotosComponent,
  path: '',
  children
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotosRoutingModule {
}
