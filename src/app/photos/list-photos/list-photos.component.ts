import {Component, OnInit, ViewChild} from '@angular/core';
import {GetMediasGQL} from '@generated/graphql';
import {map} from 'rxjs/operators';
import {NgxMasonryOptions} from 'ngx-masonry/lib/ngx-masonry-options';
import {NgxMasonryComponent} from 'ngx-masonry';

@Component({
  selector: 'app-list-photos',
  templateUrl: './list-photos.component.html',
  styleUrls: ['./list-photos.component.scss']
})
export class ListPhotosComponent implements OnInit {

  public readonly width = 256;
  public readonly options: NgxMasonryOptions = {gutter: 20};
  public medias$ = this.getMediasGQL.fetch().pipe(
    map(m => m.data?.medias),
  );

  @ViewChild(NgxMasonryComponent) private masonry: NgxMasonryComponent;

  constructor(
    protected readonly getMediasGQL: GetMediasGQL,
  ) {
  }

  ngOnInit(): void {
  }

}
