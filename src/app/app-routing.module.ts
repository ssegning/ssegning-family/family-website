import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '@shared/guard/auth.guard';
import {NonAuthGuard} from '@shared/guard/non-auth.guard';
import {FamilyResolver} from '@family/resolvers/family.resolver';

const routes: Routes = [
  {
    loadChildren: () => import('./non-auth/non-auth.module').then(m => m.NonAuthModule),
    path: 'join',
    canActivate: [NonAuthGuard],
  },
  {
    loadChildren: () => import('./main/main.module').then(m => m.MainModule),
    path: 'family/:familyName',
    canActivate: [AuthGuard],
    data: {
      role: []
    },
    resolve: {
      family: FamilyResolver
    }
  },
  {
    loadChildren: () => import('./family/family.module').then(m => m.FamilyModule),
    path: 'family',
    canActivate: [AuthGuard],
    data: {
      role: []
    }
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'family'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
