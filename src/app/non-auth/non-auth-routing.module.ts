import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NonAuthComponent} from './non-auth/non-auth.component';

const routes: Routes = [{
  component: NonAuthComponent,
  path: '',
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NonAuthRoutingModule {
}
