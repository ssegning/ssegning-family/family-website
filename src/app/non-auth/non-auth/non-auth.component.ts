import {Component, OnInit} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';

@Component({
  selector: 'app-non-auth',
  templateUrl: './non-auth.component.html',
  styleUrls: ['./non-auth.component.scss']
})
export class NonAuthComponent implements OnInit {

  constructor(
    protected readonly keycloakService: KeycloakService
  ) {
  }

  ngOnInit(): void {
  }

  async login(): Promise<void> {
    await this.keycloakService.login({redirectUri: window.location.origin});
  }

}
