import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NonAuthRoutingModule } from './non-auth-routing.module';
import { NonAuthComponent } from './non-auth/non-auth.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [NonAuthComponent],
  imports: [
    CommonModule,
    NonAuthRoutingModule,
    SharedModule
  ]
})
export class NonAuthModule { }
