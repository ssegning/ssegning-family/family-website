# Mifa Web Apps

### Docs

- https://apollo-angular.com/docs/get-started
- https://apollo-angular.com/docs/recipes/automatic-persisted-queries
- https://graphql-code-generator.com/docs/plugins/typescript-apollo-angular
- https://stackoverflow.com/questions/49670232
- https://stackoverflow.com/questions/46257184

### Commands

```bash
helm install family-website ./family-website -n family
```

```bash
helm upgrade family-website ./family-website -n family
```

```bash
oc start-build family-website-build -n family
```
